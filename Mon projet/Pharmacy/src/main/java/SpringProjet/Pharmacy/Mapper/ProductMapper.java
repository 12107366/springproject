package SpringProjet.Pharmacy.Mapper;

import SpringProjet.Pharmacy.Data.ProductData;
import SpringProjet.Pharmacy.Entities.Product;
import org.springframework.stereotype.Component;
@Component
public class ProductMapper {
    public static Product toEntity(ProductData productData) {
        Product product = new Product();
        product.setName(productData.getName());
        product.setPrice((float) productData.getPrice());
        product.setQuantity(productData.getQuantity());
        return product;
    }
}

