package SpringProjet.Pharmacy.Mapper;

import SpringProjet.Pharmacy.Data.BillData;
import SpringProjet.Pharmacy.Entities.Bill;
import org.springframework.stereotype.Component;

@Component
public class BillMapper {
    public Bill toEntity(BillData billData) {
        Bill bill = new Bill();
        bill.setId(billData.getId());
        return bill;
    }

    public BillData toData(Bill bill) {
        return BillData.builder()
                .id(bill.getId())
                .build();
    }
}

