package SpringProjet.Pharmacy.Mapper;

import SpringProjet.Pharmacy.Data.ProductCartData;
import SpringProjet.Pharmacy.Entities.Cart;
import SpringProjet.Pharmacy.Entities.Product;
import SpringProjet.Pharmacy.Entities.ProductCart;
import SpringProjet.Pharmacy.Repositories.CartRepository;
import SpringProjet.Pharmacy.Repositories.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductCartMapper {

    @Autowired
    private ProductRepository productRepository;

    public ProductCart toEntity(ProductCartData productCartData) {
        ProductCart productCart = new ProductCart();
        productCart.setId(productCartData.getId());
        Product product = productRepository.findById(productCartData.getProduitId())
                .orElseThrow(() -> new EntityNotFoundException("Product not found with ID: " + productCartData.getProduitId()));
        productCart.setProduct(product);
        productCart.setQuantity(productCartData.getQuantity());
        return productCart;
    }
    public ProductCartData toDto(ProductCart productCart) {
        ProductCartData productCartData = new ProductCartData();
        productCartData.setId(productCart.getId());
        productCartData.setPanierId(productCart.getCart().getId());
        productCartData.setProduitId(productCart.getProduct().getId());
        productCartData.setQuantity(productCart.getQuantity());
        return productCartData;
    }
}

