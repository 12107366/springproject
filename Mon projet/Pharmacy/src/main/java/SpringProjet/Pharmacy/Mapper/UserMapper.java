package SpringProjet.Pharmacy.Mapper;
import SpringProjet.Pharmacy.Data.UserData;
import SpringProjet.Pharmacy.Entities.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User toEntity(UserData userData) {
        if (userData == null) {
            return null;
        }
        User user = new User();
        user.setFirstname(userData.getFirstName());
        user.setLastname(userData.getLastName());
        user.setEmail(userData.getEmail());
        return user;
    }
}
