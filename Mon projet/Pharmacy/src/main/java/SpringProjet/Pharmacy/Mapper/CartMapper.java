package SpringProjet.Pharmacy.Mapper;

import SpringProjet.Pharmacy.Data.CartData;
import SpringProjet.Pharmacy.Entities.Cart;
import SpringProjet.Pharmacy.Entities.User;
import SpringProjet.Pharmacy.Repositories.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class CartMapper {

    @Autowired
    private UserRepository userRepository; // Supposons que vous avez un UserRepository injecté

    public CartData toDto(Cart cart) {
        CartData cartData = new CartData();
        cartData.setProductId(cart.getUser().getId());
        cartData.setUserId(cart.getId());
        return cartData;
    }

    public Cart toEntity(CartData cartData) {
        Cart cart = new Cart();
        cart.setId(cartData.getUserId());

        User user = userRepository.findById(cartData.getProductId())
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        cart.setUser(user);

        return cart;
    }
}