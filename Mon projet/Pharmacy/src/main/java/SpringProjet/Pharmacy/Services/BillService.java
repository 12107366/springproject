package SpringProjet.Pharmacy.Services;
import SpringProjet.Pharmacy.Entities.Bill;
import SpringProjet.Pharmacy.Repositories.BillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Optional;
@Service
public class BillService {

    private static BillRepository billRepository;

    @Autowired
    public void setBillRepository(BillRepository billRepository) {
        BillService.billRepository = billRepository;
    }

    public static Bill createBill(float total, Long userId) {
        Bill bill = new Bill();
        bill.setTotal(total);
        bill.setUser(userId);
        bill.setDate(new Date());
        return billRepository.save(bill);
    }

    public static Bill getBill(Long billId) {
        Optional<Bill> optionalBill = billRepository.findById(billId);
        return optionalBill.orElse(null);
    }
}