package SpringProjet.Pharmacy.Services;
import SpringProjet.Pharmacy.Entities.Cart;
import SpringProjet.Pharmacy.Entities.Product;
import SpringProjet.Pharmacy.Entities.ProductCart;
import SpringProjet.Pharmacy.Entities.User;
import SpringProjet.Pharmacy.Repositories.CartRepository;
import SpringProjet.Pharmacy.Repositories.ProductCartRepository;
import SpringProjet.Pharmacy.Repositories.ProductRepository;
import SpringProjet.Pharmacy.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
@Service
public class CartService {

    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final CartRepository cartRepository;
    private final ProductCartRepository productCartRepository;

    @Autowired
    public CartService(ProductRepository productRepository,
                       UserRepository userRepository,
                       CartRepository cartRepository,
                       ProductCartRepository productCartRepository) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.cartRepository = cartRepository;
        this.productCartRepository = productCartRepository;
    }

    public void addToCart(Long userId, Long productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent() && productOptional.isPresent()) {
            User user = userOptional.get();
            Product product = productOptional.get();
            Optional<Cart> cartOptional = cartRepository.findByUser(user);
            Cart cart;
            if (cartOptional.isPresent()) {
                cart = cartOptional.get();
            } else {
                cart = new Cart();
                cart.setUser(user);
                cart = cartRepository.save(cart);
            }
            Optional<ProductCart> productCartOptional = productCartRepository.findByCartAndProduct(cart, product);
            ProductCart productCart;
            if (productCartOptional.isPresent()) {
                productCart = productCartOptional.get();
                productCart.setQuantity(productCart.getQuantity() + 1);
            } else {
                productCart = new ProductCart();
                productCart.setCart(cart);
                productCart.setProduct(product);
                productCart.setQuantity(1);
            }
            productCartRepository.save(productCart);
        } else {
            throw new EntityNotFoundException("User or product not found.");
        }
    }

    public void removeFromCart(Long userId, Long productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent() && productOptional.isPresent()) {
            User user = userOptional.get();
            Product product = productOptional.get();
            Optional<Cart> cartOptional = cartRepository.findByUser(user);
            if (cartOptional.isPresent()) {
                Cart cart = cartOptional.get();
                Optional<ProductCart> productCartOptional = productCartRepository.findByCartAndProduct(cart, product);
                if (productCartOptional.isPresent()) {
                    ProductCart productCart = productCartOptional.get();
                    if (productCart.getQuantity() > 1) {
                        productCart.setQuantity(productCart.getQuantity() - 1);
                        productCartRepository.save(productCart);
                    } else {
                        productCartRepository.delete(productCart);
                    }
                } else {
                    throw new EntityNotFoundException("Product not found in the cart.");
                }
            } else {
                throw new EntityNotFoundException("Cart not found for the user.");
            }
        } else {
            throw new EntityNotFoundException("User or product not found.");
        }
    }

    public List<Product> getCart(Long userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            Optional<Cart> cartOptional = cartRepository.findByUser(user);
            if (cartOptional.isPresent()) {
                Cart cart = cartOptional.get();
                return productCartRepository.findProductsByCart(cart);
            } else {
                throw new EntityNotFoundException("Cart not found for the user.");
            }
        } else {
            throw new EntityNotFoundException("User not found.");
        }
    }
}


