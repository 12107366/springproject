package SpringProjet.Pharmacy.Services;
import SpringProjet.Pharmacy.Repositories.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import SpringProjet.Pharmacy.Entities.Product;

@Service
public class StockService {

    private final ProductRepository productRepository;

    @Autowired
    public StockService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public void deleteProduit(Long id) {
        if (productRepository.existsById(id)) {
            productRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException("Le medicament" + id + " n'existe pas dans le stock");
        }
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProductById(Long id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Le medicament " + id + " n'existe pas dans le stock."));
    }
}
