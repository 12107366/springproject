package SpringProjet.Pharmacy.Repositories;
import SpringProjet.Pharmacy.Entities.Bill;
import SpringProjet.Pharmacy.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BillRepository extends JpaRepository<Bill, Long> {
    Optional<Bill> findById(Long id);
    List<Bill> findAllByUser(User id_user);
    List<Bill> findAll();
    void deleteById(Long id);
}