package SpringProjet.Pharmacy.Repositories;
import SpringProjet.Pharmacy.Entities.Cart;
import SpringProjet.Pharmacy.Entities.Product;
import SpringProjet.Pharmacy.Entities.ProductCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductCartRepository extends JpaRepository<ProductCart, Long> {
    Optional<ProductCart> findByCartAndProduct(Cart cart, Product product);
    List<Product> findProductsByCart(Cart cart);
}
