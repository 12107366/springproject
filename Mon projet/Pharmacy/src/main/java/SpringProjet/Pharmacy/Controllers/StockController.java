package SpringProjet.Pharmacy.Controllers;
import SpringProjet.Pharmacy.Data.ProductData;
import SpringProjet.Pharmacy.Entities.Product;
import SpringProjet.Pharmacy.Services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/stock")
public class StockController {

    private final StockService stockService;

    @Autowired
    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @PostMapping("/product")
    public ResponseEntity<Product> addProduct(@RequestBody ProductData productData) {
        Product product = mapProductDataToProduct(productData);
        Product savedProduct = stockService.addProduct(product);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedProduct);
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable("id") Long id) {
        Product product = stockService.getProductById(id);
        return ResponseEntity.ok(product);
    }

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getProducts() {
        List<Product> products = stockService.getAllProducts();
        return ResponseEntity.ok(products);
    }



    @DeleteMapping("/product/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable("id") Long id) {
        stockService.deleteProduit(id);
        return ResponseEntity.noContent().build();
    }

    private Product mapProductDataToProduct(ProductData productData) {
        Product product = new Product();
        product.setName(productData.getName());
        product.setPrice(productData.getPrice());
        product.setQuantity(productData.getQuantity());
        return product;
    }
}
