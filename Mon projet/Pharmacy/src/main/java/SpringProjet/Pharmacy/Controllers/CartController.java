package SpringProjet.Pharmacy.Controllers;
import SpringProjet.Pharmacy.Data.CartData;
import SpringProjet.Pharmacy.Entities.Product;
import SpringProjet.Pharmacy.Services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/panier")
public class CartController {
    private final CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PostMapping("/add")
    public ResponseEntity<Void> addToCart(@RequestBody CartData cartData) {
        cartService.addToCart(cartData.getUserId(), cartData.getProductId());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/supprimer/{userId}/{productId}")
    public ResponseEntity<Void> removeFromCart(@PathVariable("userId") Long userId, @PathVariable("productId") Long productId) {
        cartService.removeFromCart(userId, productId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<Product>> getCart(@PathVariable("userId") Long userId) {
        List<Product> cart = cartService.getCart(userId);
        return ResponseEntity.ok(cart);
    }

    @GetMapping("/string")
    public ResponseEntity<String> getMessage() {
        String message = "Welcome to your cart";
        return ResponseEntity.ok(message);
    }
}
