package SpringProjet.Pharmacy.Controllers;
import SpringProjet.Pharmacy.Entities.Bill;
import SpringProjet.Pharmacy.Services.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/facturation")
public class BillController {

    private final BillService BillService;

    @Autowired
    public BillController(BillService BillService) {
        this.BillService = BillService;
    }

    @PostMapping("/creer")
    public ResponseEntity<Bill> createBill(@RequestParam float total, @RequestParam Long userId) {
        Bill bill = BillService.createBill(total, userId);
        return ResponseEntity.status(HttpStatus.CREATED).body(bill);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Bill> getBill(@PathVariable("id") Long id) {
        Bill bill = BillService.getBill(id);
        return ResponseEntity.ok(bill);
    }
}

