package SpringProjet.Pharmacy.Controllers;

import SpringProjet.Pharmacy.Data.UserData;
import SpringProjet.Pharmacy.Entities.User;
import SpringProjet.Pharmacy.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<User> addUser(@RequestBody UserData userData) {
        User user = new User();
        user.setFirstname(userData.getFirstName());
        user.setLastname(userData.getLastName());
        user.setEmail(userData.getEmail());
        user.setActive(true);
        User savedUser = userService.addUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedUser);
    }

    @GetMapping
    public ResponseEntity<List<User>> getUsers() {
        List<User> users = userService.getAllUsers();
        return ResponseEntity.ok(users);
    }

}
