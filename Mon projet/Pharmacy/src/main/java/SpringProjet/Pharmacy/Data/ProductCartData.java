package SpringProjet.Pharmacy.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ProductCartData {
    private Long id;
    private Long panierId;
    private Long produitId;
    private int quantity;

    public ProductCartData() {
    }

    public ProductCartData(Long id, Long panierId, Long produitId, int quantity) {
        this.id = id;
        this.panierId = panierId;
        this.produitId = produitId;
        this.quantity = quantity;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPanierId() {
        return panierId;
    }

    public void setPanierId(Long panierId) {
        this.panierId = panierId;
    }

    public Long getProduitId() {
        return produitId;
    }

    public void setProduitId(Long produitId) {
        this.produitId = produitId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
