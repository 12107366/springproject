package SpringProjet.Pharmacy.Data;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserData {
    private String firstName;
    private String lastName;
    private String email;
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String nom) {
        this.firstName = nom;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
}
