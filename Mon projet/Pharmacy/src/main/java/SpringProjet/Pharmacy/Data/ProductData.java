package SpringProjet.Pharmacy.Data;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProductData {
    @NotNull(groups = ProductUpdate.class)
    private Long id;

    @NotBlank(groups = {ProductCreation.class, ProductUpdate.class})
    private String name;

    @NotNull(groups = {ProductCreation.class, ProductUpdate.class})
    private float price;

    @NotNull(groups = {ProductCreation.class, ProductUpdate.class})
    private int quantity;

    public interface ProductCreation {}
    public interface ProductUpdate {}
}
