package SpringProjet.Pharmacy.Data;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CartData {
    private Long productId;
    private Long userId;

    public CartData(Long productId, Long userId) {
        this.productId = productId;
        this.userId = userId;
    }
    public CartData() {

    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }
}
